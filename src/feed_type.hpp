#pragma once

enum FeedType {
    INVALID, RSS, ATOM, RDF
};
